import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'models/Connection.dart';
import 'viewmodels/settingviewmodel.dart';
import 'ui/login.dart';
import 'ui/mytheme.dart';
import 'viewmodels/task_view_model.dart';

void main() {
  runApp(const MyTD2());
}

class MyTD2 extends StatelessWidget {
  const MyTD2({super.key});

  @override
  Widget build(BuildContext context) {
    Connection c = Connection();
    c.connection();
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) {
          SettingViewModel settingViewModel = SettingViewModel();
          //getSettings est deja appelee dans le constructeur
          return settingViewModel;
        }),
        ChangeNotifierProvider(create: (_) {
          TaskViewModel taskViewModel = TaskViewModel(c);
          taskViewModel.generateTasks();
          return taskViewModel;
        })
      ],
      child: Consumer<SettingViewModel>(
        builder: (context, SettingViewModel notifier, child) {
          return MaterialApp(
              debugShowCheckedModeBanner: false,
              theme: notifier.isDark ? MyTheme.dark() : MyTheme.light(),
              title: 'Lou-lou',
              home: Login(c));
        },
      ),
    );
  }
}
