import 'dart:convert';

class Todo {
  int _id;
  String _title;
  String _description;
  double _price;
  String _img;
  double _rate;
  int _count;

  Todo(this._id, this._title, this._description, this._price, this._img,
      this._rate, this._count);

  String get description => _description;

  String get title => _title;

  String get img => _img;

  double get price => _price;

  int get id => _id;

  int get count => _count;

  double get rate => _rate;

  factory Todo.fromJson(dynamic json) {
    int id = json['id'] ?? 0;
    String title = json['title'] ?? "";
    String img = json['image'] ?? "";
    double price = json['price'].toDouble() ?? 0.0;
    String description = json['description'] ?? false;
    double rate = json['rating']['rate'].toDouble() ?? 0;
    int count = json['rating']['count'] ?? 0;
    return Todo(id, title, description, price, img, rate, count);
  }

  void modifier(
      String title, String description, double price, double rate, int count) {
    _title = title;
    _description = description;
    _price = price;
    _rate = rate;
    _count = count;
  }
}
