// ignore_for_file: must_be_immutable, no_logic_in_create_state

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:settings_ui/settings_ui.dart';
import '../ui/login.dart';
import '../ui/mytheme.dart';
import '../ui/profil.dart';
import '../viewmodels/settingviewmodel.dart';
import 'Connection.dart';

class EcranSettings extends StatefulWidget {
  Map<String, String?>? user;
  Connection c;
  EcranSettings(this.c, this.user, {super.key});
  @override
  State<EcranSettings> createState() => _EcranSettingsState(c, user);
}

class _EcranSettingsState extends State<EcranSettings> {
  Map<String, String?>? user;
  Connection c;
  _EcranSettingsState(this.c, this.user);
  @override
  Widget build(BuildContext context) {
    return Center(
      child: SettingsList(
        darkTheme: SettingsThemeData(
            settingsListBackground: MyTheme.dark().scaffoldBackgroundColor,
            settingsSectionBackground: MyTheme.dark().scaffoldBackgroundColor),
        lightTheme: SettingsThemeData(
            settingsListBackground: MyTheme.light().scaffoldBackgroundColor,
            settingsSectionBackground: MyTheme.light().scaffoldBackgroundColor),
        sections: [
          SettingsSection(title: const Text('Profil'), tiles: [
            SettingsTile.switchTile(
                initialValue: context.watch<SettingViewModel>().pro,
                onToggle: (bool value) {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => Profil(c, user),
                      ));
                },
                title: const Text('Profil'),
                leading: const CircleAvatar(
                  backgroundColor: Colors.grey,
                  child: Icon(
                    Icons.person,
                    color: Colors.black,
                  ),
                ))
          ]),
          SettingsSection(title: const Text('Theme'), tiles: [
            SettingsTile.switchTile(
              initialValue: context.watch<SettingViewModel>().isDark,
              onToggle: (bool value) {
                context.read<SettingViewModel>().isDark = value;
              },
              title: const Text('Dark mode'),
              leading: const Icon(Icons.invert_colors),
            )
          ]),
          SettingsSection(title: const Text('Déconnexion'), tiles: [
            SettingsTile.switchTile(
              initialValue: context.watch<SettingViewModel>().co,
              onToggle: (bool value) {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => Login(c),
                    ));
              },
              title: const Text('Se deconnecter'),
              leading: const Icon(Icons.door_back_door),
            )
          ]),
        ],
      ),
    );
  }
}
