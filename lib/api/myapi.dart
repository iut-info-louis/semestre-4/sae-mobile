import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import '../models/todos.dart';

class MyAPI {
  Future<String> _loadAsset(String path) async {
    return rootBundle.loadString(path);
  }

  Future<List<Todo>> getTodos() async {
    final response =
    await http.get(Uri.parse('https://fakestoreapi.com/products'));
    if (response.statusCode == 200) {
      final List<dynamic> json = jsonDecode(response.body);
      final todos = <Todo>[];
      json.forEach((element) {
        todos.add(Todo.fromJson(element));
      });
      return todos;
    } else {
      throw Exception('Failed to load todos');
    }
  }

  void deleteTodos(int id) async {
    final response =
    await http.delete(Uri.parse('https://fakestoreapi.com/products/${id}'));
    if (response.statusCode == 200) {
      print('Suppression ok');
    } else {
      throw Exception('Failed to delete todos');
    }
  }

  static void addTodos(Todo todo) async {
    final response =
    await http.post(Uri.parse('https://fakestoreapi.com/products'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(<String, String>{"produit": jsonEncode(todo)}));
    if (response.statusCode == 200) {
      print('Ajout ok');
    } else {
      throw Exception('Failed to add todos');
    }
  }

  static void mofifierTodo(int id, String titre, String description,
      String lienImg, double price) async {

    String leJson = jsonEncode(
      {
        "titre": titre,
        "price": price,
        "description": description,
        "image": lienImg,
        "category": "homme"
      },
    );
    final response =
    await http.post(Uri.parse('https://fakestoreapi.com/products/$id'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: leJson
    ) ;
    if (response.statusCode == 200) {
      print('modification ok');
    } else {
      throw Exception('Failed to modifier todos');

    }
  }
}
