import 'package:flutter/material.dart';
import '../models/SettingRepository.dart';

class SettingViewModel extends ChangeNotifier {
  late bool _isDark;
  late bool _co;
  late bool _pro;
  late SettingRepository _settingRepository;
  bool get isDark => _isDark;
  bool get co => _co;
  bool get pro => _pro;

  SettingViewModel() {
    _isDark = true;
    _co = false;
    _pro = false;
    _settingRepository = SettingRepository();
    getSettings();
  }

//Switching the themes
  set isDark(bool value) {
    _isDark = value;
    _settingRepository.saveSettings(value);
    notifyListeners();
  }

  set isPro(bool value) {
    _pro = value;
    _settingRepository.saveSettings(value);
    notifyListeners();
  }

  set conection(bool value) {
    _co = value;
    _settingRepository.saveSettings(value);
    notifyListeners();
  }

  getSettings() async {
    _isDark = await _settingRepository.getSettings();
    notifyListeners();
  }
}
