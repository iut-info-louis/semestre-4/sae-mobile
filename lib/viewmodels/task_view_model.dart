import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import '../models/Connection.dart';
import '../models/todos.dart';

class TaskViewModel extends ChangeNotifier {
  Connection c;
  late List<Todo> liste; // late permet de ne pas initialiser la variable
  TaskViewModel(this.c) {
    liste = [];
  }

  Todo? getTask(int id) {
    for (Todo task in liste) {
      if (task.id == id) return task;
    }
    return null;
  }

  Future<List<Todo>> getPanier() async {
    List<Todo> liste = [];
    int i = 0;
    List<Map<String, String?>?> response =
    await c.requet("select * from PANIER");
    print(response);
    while (i < response.length) {
      print(int.parse(response[i]!["idT"]!));
      Todo? todo = getTask(int.parse(response[i]!["idT"]!));
      liste.add(todo!);
      i = i + 1;
    }
    print(liste);
    return liste;
  }

  Future<List<Todo>> getFavoris() async {
    List<Todo> liste = [];
    int i = 0;
    List<Map<String, String?>?> response =
    await c.requet("select * from FAVORIS");
    while (i < response.length) {
      Todo? todo = getTask(int.parse(response[i]!["idT"]!));
      liste.add(todo!);
      i = i + 1;
    }
    return liste;
  }

  Future<List<Todo>> getHistorique() async {
    List<Todo> liste = [];
    int i = 0;
    List<Map<String, String?>?> response =
    await c.requet("select * from HISTORIQUE");
    while (i < response.length) {
      Todo? todo = getTask(int.parse(response[i]!["idT"]!));
      liste.add(todo!);
      i = i + 1;
    }
    return liste;
  }

  List<Todo> getTasks() {
    return liste;
  }

  void addTask(Todo task) {
    liste.add(task);
    notifyListeners(); // permet de notifier les listeners
  }

  void modifierTask(Todo? task, String title, String description, double price,
      double rate, int count) {
    task?.modifier(title, description, price, rate, count);
    print("bonjour");
  }

  void deleteTask(task) {
    liste.remove(task);
  }

  Future<void> generateTasks() async {
    final response =
    await http.get(Uri.parse('https://fakestoreapi.com/products'));
    if (response.statusCode == 200) {
      final List<dynamic> json = jsonDecode(response.body);
      final todos = <Todo>[];
      for (var element in json) {
        todos.add(Todo.fromJson(element));
      }
      liste = todos;
      notifyListeners();
    }
  }
}