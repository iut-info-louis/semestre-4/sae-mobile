import 'package:flutter/material.dart';
import '../models/Connection.dart';
import '../models/checkbox.dart';
import 'home.dart';

class Acceuil extends StatelessWidget {
  Map<String, String?> user;
  Connection c;
  Acceuil(this.c, this.user, {super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text(
            'Lou-lou',
            style: TextStyle(
                color: Colors.black, fontWeight: FontWeight.w500, fontSize: 30),
          ),
        ),
        body: Card(
            child: Column(
              children: <Widget>[
                const ListTile(
                    title: Text(
                      "Bien venu sur note application d'achat. Elle vous permet de consulter une liste de produit et de les acheter si vous le souhaitez. Si vous ne voulez plus que ce message s'affiche cocher la case. Pour acceder a l'application cliquer sur la bouton Continuer",
                      textDirection: TextDirection.ltr,
                      style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontStyle: FontStyle.italic,
                      ),
                    )),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    TextButton(
                      child: const Text("Continuer"),
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => Home(c, user),
                            ));
                      },
                    ),
                    const SizedBox(width: 8),
                    MyStatefulWidget(),
                  ],
                ),
              ],
            )));
  }
}
