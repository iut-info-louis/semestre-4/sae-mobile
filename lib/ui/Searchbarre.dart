// ignore_for_file: unnecessary_this,ignore_for_file: use_key_in_widget_constructors, must_be_immutable, no_logic_in_create_state, library_private_types_in_public_api, file_names, curly_braces_in_flow_control_structures

import 'package:anim_search_bar/anim_search_bar.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sae_mobile/ui/Favoris.dart';
import 'package:sae_mobile/ui/Panier.dart';

import '../models/Connection.dart';
import '../models/todos.dart';
import '../viewmodels/task_view_model.dart';
import 'consultation.dart';
import 'home.dart';

class Searchbarre extends StatefulWidget {
  Map<String, String?>? user;
  late List<Todo> tasks = <Todo>[];
  String string;
  Connection c;
  Searchbarre(this.tasks, this.string, this.c, this.user, {super.key});

  @override
  _Searchbarre createState() => _Searchbarre(tasks, string, c, user);
}

class _Searchbarre extends State<Searchbarre> {
  Map<String, String?>? user;
  TextEditingController textController = TextEditingController();
  String image = "";
  late List<Todo> todos = <Todo>[];
  String string;
  Connection c;
  _Searchbarre(this.todos, this.string, this.c, this.user);
  List<Todo> liste = [];

  @override
  Widget build(BuildContext context) {
    TaskViewModel listeTodo = context.watch<TaskViewModel>();
    for (Todo todo in todos) {
      if (todo.title.contains(string)) {
        liste.add(todo);
      }
    }

    return Scaffold(
        appBar: AppBar(
          actions: <Widget>[
            AnimSearchBar(
              autoFocus: true,
              width: 400,
              textController: textController,
              onSuffixTap: () {
                setState(() {
                  textController.clear();
                });
              },
              onSubmitted: (String) {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) =>
                          Searchbarre(listeTodo.getTasks(), String, c, user),
                    ));
              },
            ),
            IconButton(
              icon: const Icon(
                Icons.favorite,
                color: Colors.pink,
              ),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => Favoris(c, listeTodo, user),
                    ));
              },
            ),
            IconButton(
              icon: const Icon(
                Icons.shopping_cart,
                color: Colors.black,
              ),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => Panier(c, listeTodo, user),
                    ));
              },
            ),
            IconButton(
              icon: const Icon(
                Icons.home,
                color: Colors.grey,
              ),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => Home(c, user),
                    ));
              },
            ),
            Container(
                padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                child: CircleAvatar(
                  backgroundColor: Colors.grey[200],
                  radius: 48,
                  backgroundImage: NetworkImage(user!["avatar"]!),
                )),
          ],
          title: const Text(
            'Application Push',
            style: TextStyle(
                color: Colors.black, fontWeight: FontWeight.w500, fontSize: 30),
          ),
        ),
        body: ListView.builder(
            itemCount: liste.length,
            itemBuilder: (context, index) => Card(
              elevation: 6,
              margin: const EdgeInsets.all(10),
              child: ListTile(
                leading: CircleAvatar(
                  backgroundColor: Colors.white,
                  child: Image.network(
                    liste[index].img,
                    height: 50,
                  ),
                ),
                title: Text(liste[index].title),
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => MyWidget(
                            c: c,
                            todo: liste[index],
                            listeTodos: listeTodo,
                            user: user),
                      ));
                },
              ),
            )));
  }
}
