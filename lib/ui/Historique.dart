// ignore_for_file: sort_child_properties_last, no_logic_in_create_state, library_private_types_in_public_api, must_be_immutable, file_names

import 'package:anim_search_bar/anim_search_bar.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../models/Connection.dart';
import '../models/todos.dart';
import '../viewmodels/task_view_model.dart';
import 'Favoris.dart';
import 'Panier.dart';
import 'Searchbarre.dart';
import 'consultation.dart';
import 'home.dart';

class Historique extends StatefulWidget {
  Connection c;
  TaskViewModel objTasks;
  Map<String, String?>? user;
  Historique(this.c, this.objTasks, this.user, {super.key});

  @override
  _Historique createState() => _Historique(c, objTasks, user);
}

class _Historique extends State<Historique> {
  Connection c;
  TaskViewModel objTasks;
  Map<String, String?>? user;
  _Historique(this.c, this.objTasks, this.user);
  late Future<List<Todo>> futureTodo;
  @override
  void initState() {
    super.initState();
    futureTodo = objTasks.getHistorique();
  }

  @override
  Widget build(BuildContext context) {
    TaskViewModel listeTodo = context.watch<TaskViewModel>();

    return Scaffold(
        body: FutureBuilder(
            future: futureTodo,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return ListView.builder(
                    itemCount: snapshot.data!.length,
                    itemBuilder: (context, index) => Card(
                      elevation: 6,
                      margin: const EdgeInsets.all(10),
                      child: ListTile(
                        leading: CircleAvatar(
                          backgroundColor: Colors.white,
                          child: Image.network(
                            snapshot.data![index].img,
                            height: 50,
                          ),
                        ),
                        title: Text(snapshot.data![index].title),
                        trailing: IconButton(
                          iconSize: 30,
                          icon: const Icon(
                            Icons.delete,
                            color: Colors.black,
                          ),
                          onPressed: () {
                            c.requet(
                                "delete from HISTORIQUE where idT = ${snapshot.data![index].id}");
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => Home(c, user),
                                ));
                          },
                        ),
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => MyWidget(
                                    c: c,
                                    listeTodos: objTasks,
                                    todo: snapshot.data![index],
                                    user: user),
                              ));
                        },
                      ),
                    ));
              } else if (snapshot.hasError) {
                return Text('${snapshot.error}');
              } else {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              }
            }));
  }
}
