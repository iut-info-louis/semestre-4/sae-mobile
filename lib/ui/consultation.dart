// ignore_for_file: prefer_const_constructors, must_be_immutable, dead_code, sort_child_properties_last, no_logic_in_create_state, library_private_types_in_public_api
import 'package:anim_search_bar/anim_search_bar.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sae_mobile/api/myapi.dart';
import 'package:sae_mobile/models/todos.dart';

import '../models/Connection.dart';
import '../viewmodels/task_view_model.dart';
import 'Favoris.dart';
import 'Panier.dart';
import 'Searchbarre.dart';
import 'home.dart';
import 'modifier.dart';

class MyWidget extends StatefulWidget {
  Map<String, String?>? user;
  late Connection c;
  final Todo todo;
  final TaskViewModel listeTodos;

  MyWidget({
    required this.todo,
    required this.listeTodos,
    required this.c,
    required this.user,
  });

  @override
  _MyWidget createState() =>
      _MyWidget(c: c, todo: todo, listeTodos: listeTodos, user: user);
}

class _MyWidget extends State<MyWidget> {
  Map<String, String?>? user;
  late Connection c;
  final Todo todo;
  final TaskViewModel listeTodos;

  _MyWidget({
    required this.todo,
    required this.listeTodos,
    required this.c,
    required this.user,
  });
  @override
  Widget build(BuildContext context) {
    MyAPI myAPI = MyAPI();
    TextEditingController nom = TextEditingController(text: todo.title);
    TextEditingController textController = TextEditingController();
    TextEditingController desc = TextEditingController(text: todo.description);
    TextEditingController avis = TextEditingController(
      text: "${todo.rate} sur ${todo.count.toString()} avis",
    );
    TextEditingController prix = TextEditingController(
      text: "${todo.price.toString()} euros",
    );
    TextEditingController img = TextEditingController(text: todo.img);
    TaskViewModel listeTodo = context.watch<TaskViewModel>();

    return Scaffold(
        appBar: AppBar(
          actions: <Widget>[
            AnimSearchBar(
              autoFocus: true,
              width: 400,
              textController: textController,
              onSuffixTap: () {
                setState(() {
                  textController.clear();
                });
              },
              onSubmitted: (String) {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) =>
                          Searchbarre(listeTodo.getTasks(), String, c, user),
                    ));
              },
            ),
            IconButton(
              icon: Icon(
                Icons.favorite,
                color: Colors.pink,
              ),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => Favoris(c, listeTodo, user),
                    ));
              },
            ),
            IconButton(
              icon: Icon(
                Icons.shopping_cart,
                color: Colors.black,
              ),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => Panier(c, listeTodo, user),
                    ));
              },
            ),
            IconButton(
              icon: const Icon(
                Icons.home,
                color: Colors.grey,
              ),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => Home(c, user),
                    ));
              },
            ),
            Container(
                padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                child: CircleAvatar(
                  backgroundColor: Colors.grey[200],
                  radius: 48,
                  backgroundImage: NetworkImage(user!["avatar"]!),
                )),
          ],
          title: Text(
            'Loulou',
            style: TextStyle(
                color: Colors.black, fontWeight: FontWeight.w500, fontSize: 30),
          ),
        ),
        body: Material(
            child: Padding(
                padding: const EdgeInsets.all(10),
                child: ListView(
                  children: <Widget>[
                    Container(
                        alignment: Alignment.center,
                        padding: const EdgeInsets.all(10),
                        child: const Text(
                          'Consultation du produit',
                          style: TextStyle(fontSize: 20),
                        )),
                    Container(
                      child: Image.network(
                        todo.img,
                        height: 200,
                        width: 300,
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.all(10),
                      child: TextField(
                        enabled: false,
                        controller: nom,
                        decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: 'Nom du produit',
                        ),
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                      child: TextFormField(
                        enabled: false,
                        minLines: 6,
                        maxLines: null,
                        controller: desc,
                        keyboardType: TextInputType.multiline,
                        decoration: const InputDecoration(
                          alignLabelWithHint: true,
                          border: OutlineInputBorder(),
                          labelText: 'Description',
                        ),
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.all(10),
                      child: TextField(
                        enabled: false,
                        controller: prix,
                        decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: 'Prix du produit',
                        ),
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.all(10),
                      child: TextField(
                        enabled: false,
                        controller: avis,
                        decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: 'Avis du produit',
                        ),
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.all(10),
                      child: TextField(
                        enabled: false,
                        controller: img,
                        decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: "Lien de l'image  du produit",
                        ),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Center(
                            child: IconButton(
                              iconSize: 30,
                              icon: Icon(
                                Icons.edit,
                                color: Colors.blue,
                              ),
                              onPressed: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => modifier(todo, c, user),
                                    ));
                              },
                            )),
                        const SizedBox(width: 8),
                        Center(
                            child: IconButton(
                              iconSize: 30,
                              icon: Icon(
                                Icons.delete,
                                color: Colors.black,
                              ),
                              onPressed: () {
                                myAPI.deleteTodos(todo.id);
                                listeTodos.deleteTask(todo);
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => Home(c, user),
                                    ));
                              },
                            )),
                        const SizedBox(width: 8),
                        Container(
                            height: 50,
                            padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                            child: ElevatedButton(
                              child: const Text('Ajouter au panier'),
                              onPressed: () {
                                c.insertPanier(todo.id);
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) =>
                                          Panier(c, listeTodo, user),
                                    ));
                              },
                            )),
                        Container(
                            height: 50,
                            padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                            child: ElevatedButton(
                              child: const Text('Ajouter au favoris'),
                              onPressed: () {
                                c.insertFavoris(todo.id);
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) =>
                                          Favoris(c, listeTodo, user),
                                    ));
                              },
                            )),
                      ],
                    ),
                  ],
                ))));
  }
}
