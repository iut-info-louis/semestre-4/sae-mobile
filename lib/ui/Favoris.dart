// ignore_for_file: sort_child_properties_last

import 'package:anim_search_bar/anim_search_bar.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sae_mobile/models/achat.dart';
import '../api/myapi.dart';
import '../models/favorite.dart';
import '../models/Connection.dart';
import '../models/todos.dart';
import '../viewmodels/task_view_model.dart';
import 'Favoris.dart';
import 'Panier.dart';
import 'Searchbarre.dart';
import 'consultation.dart';
import 'home.dart';

class Favoris extends StatefulWidget {
  Connection c;
  TaskViewModel objTasks;
  Map<String, String?>? user;
  Favoris(this.c, this.objTasks, this.user, {super.key});

  @override
  _Favoris createState() => _Favoris(c, objTasks, user);
}

class _Favoris extends State<Favoris> {
  Connection c;
  TaskViewModel objTasks;
  Map<String, String?>? user;
  _Favoris(this.c, this.objTasks, this.user);
  late Future<List<Todo>> futureTodo;
  @override
  void initState() {
    super.initState();
    futureTodo = objTasks.getFavoris();
  }

  @override
  Widget build(BuildContext context) {
    TaskViewModel listeTodo = context.watch<TaskViewModel>();
    TextEditingController textController = TextEditingController();

    return Scaffold(
        appBar: AppBar(
          actions: <Widget>[
            AnimSearchBar(
              autoFocus: true,
              width: 400,
              textController: textController,
              onSuffixTap: () {
                setState(() {
                  textController.clear();
                });
              },
              onSubmitted: (String) {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) =>
                          Searchbarre(listeTodo.getTasks(), String, c, user),
                    ));
              },
            ),
            IconButton(
              icon: const Icon(
                Icons.favorite,
                color: Colors.pink,
              ),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => Favoris(c, listeTodo, user),
                    ));
              },
            ),
            IconButton(
              icon: const Icon(
                Icons.shopping_cart,
                color: Colors.black,
              ),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => Panier(c, listeTodo, user),
                    ));
              },
            ),
            IconButton(
              icon: const Icon(
                Icons.home,
                color: Colors.grey,
              ),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => Home(c, user),
                    ));
              },
            ),
            Container(
                padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                child: CircleAvatar(
                    backgroundColor: Colors.grey[200],
                    radius: 48,
                    backgroundImage: NetworkImage(user!["avatar"]!)))
          ],
          title: const Text(
            'Lou-lou',
            style: TextStyle(
                color: Colors.black, fontWeight: FontWeight.w500, fontSize: 30),
          ),
        ),
        body: FutureBuilder(
            future: futureTodo,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return ListView.builder(
                    itemCount: snapshot.data!.length,
                    itemBuilder: (context, index) => Card(
                      elevation: 6,
                      margin: const EdgeInsets.all(10),
                      child: ListTile(
                        leading: CircleAvatar(
                          backgroundColor: Colors.white,
                          child: Image.network(
                            snapshot.data![index].img,
                            height: 50,
                          ),
                        ),
                        title: Text(snapshot.data![index].title),
                        trailing: IconButton(
                          iconSize: 30,
                          icon: const Icon(
                            Icons.delete,
                            color: Colors.black,
                          ),
                          onPressed: () {
                            c.requet(
                                "delete from FAVORIS where idT = ${snapshot.data![index].id}");
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) =>
                                      Favoris(c, listeTodo, user),
                                ));
                          },
                        ),
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => MyWidget(
                                    c: c,
                                    listeTodos: objTasks,
                                    todo: snapshot.data![index],
                                    user: user),
                              ));
                        },
                      ),
                    ));
              } else if (snapshot.hasError) {
                return Text('${snapshot.error}');
              } else {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              }
            }));
  }
}
