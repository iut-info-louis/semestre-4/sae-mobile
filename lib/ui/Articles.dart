// ignore_for_file: prefer_const_constructors, library_private_types_in_public_api, unnecessary_this, avoid_unnecessary_containers, sort_child_properties_last, duplicate_ignore

import 'package:flutter/material.dart';
import '../models/Connection.dart';
import '../models/todos.dart';
import '../viewmodels/task_view_model.dart';
import 'consultation.dart';

class Articles extends StatefulWidget {
  Map<String, String?>? user;
  Connection c;
  TaskViewModel objTasks;
  Articles(this.c, this.objTasks, this.user, {super.key});

  @override
  _ArticlesState createState() => _ArticlesState(c, objTasks, user);
}

class _ArticlesState extends State<Articles> {
  Map<String, String?>? user;
  Connection c;
  TaskViewModel objTasks;
  _ArticlesState(this.c, this.objTasks, this.user);

  @override
  Widget build(BuildContext context) {
    List<Todo> todos = objTasks.getTasks();

    return ListView.builder(
        itemCount: todos.length,
        itemBuilder: (context, index) => Card(
          elevation: 6,
          margin: const EdgeInsets.all(10),
          child: ListTile(
            leading: CircleAvatar(
              backgroundColor: Colors.white,
              child: Image.network(
                todos[index].img,
                height: 50,
              ),
            ),
            title: Text(todos[index].title),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => MyWidget(
                        c: c,
                        listeTodos: objTasks,
                        todo: todos[index],
                        user: user),
                  ));
            },
          ),
        ));
  }
}
