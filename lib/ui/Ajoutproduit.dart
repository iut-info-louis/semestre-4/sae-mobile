// ignore_for_file: sort_child_properties_last, must_be_immutable, unnecessary_const, unnecessary_new

import 'package:flutter/material.dart';
import 'package:sae_mobile/ui/home.dart';
import '../models/todos.dart';
import '../api/myapi.dart';
import '../models/Connection.dart';

class Ajoutproduit extends StatelessWidget {
  Connection c;
  Map<String, String?> user;
  Ajoutproduit(this.c, this.user, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    TextEditingController nom = TextEditingController();
    TextEditingController desc = TextEditingController();
    TextEditingController prix = TextEditingController();
    TextEditingController img = TextEditingController();
    final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
    return Scaffold(
        body: Form(
            key: _formKey,
            child: Material(
                child: Padding(
                    padding: const EdgeInsets.all(10),
                    child: ListView(
                      children: <Widget>[
                        Container(
                            alignment: Alignment.center,
                            padding: const EdgeInsets.all(10),
                            child: const Text(
                              'Ajouter produit',
                              style: TextStyle(fontSize: 20),
                            )),
                        Container(
                          padding: const EdgeInsets.all(10),
                          child: TextFormField(
                            controller: nom,
                            decoration: const InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'Nom du produit',
                              hintText: 'Entrer le nom du produit',
                            ),
                            validator: (String? value) {
                              if (value == null || value.isEmpty) {
                                return 'Veuillez entrer le nom du produit';
                              }
                              return null;
                            },
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                          child: TextFormField(
                            minLines: 6,
                            maxLines: null,
                            controller: desc,
                            keyboardType: TextInputType.multiline,
                            decoration: const InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'Description du produit',
                              hintText: 'Entrer une description du produit',
                            ),
                            validator: (String? value) {
                              if (value == null || value.isEmpty) {
                                return 'Veuillez entrer une description du produit';
                              }
                              return null;
                            },
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.all(10),
                          child: TextFormField(
                            decoration: const InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'Prix du produit',
                              hintText: 'Entrer le prix du produit',
                            ),
                            validator: (String? value) {
                              if (value == null || value.isEmpty) {
                                return 'Veuillez entrer le prix du produit';
                              }
                              return null;
                            },
                            controller: prix,
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.all(10),
                          child: TextFormField(
                            decoration: const InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: "Lien de l'image",
                              hintText: "Entrer le lien de l'image du produit",
                            ),
                            validator: (String? value) {
                              if (value == null || value.isEmpty) {
                                return "Veuillez entrer le lien de l'image du produit";
                              }
                              return null;
                            },
                            controller: img,
                          ),
                        ),
                        Container(
                          height: 50,
                          padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                          child: ElevatedButton(
                            onPressed: () {
                              if (_formKey.currentState!.validate()) {
                                Todo article = new Todo(55, nom.text, desc.text, double.parse(prix.text), img.text, 0, 0); // il faut mettre le id en dynamic
                                MyAPI.addTodos(article);
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) =>
                                          Home(c, user),
                                    ));
                              }
                            },
                            child: const Text('Enregistrer'),
                          ),
                        ),
                      ],
                    )))));
  }
}
