// ignore_for_file: sort_child_properties_last, must_be_immutable, no_logic_in_create_state, avoid_unnecessary_containers

import 'package:anim_search_bar/anim_search_bar.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sae_mobile/ui/Favoris.dart';
import 'package:sae_mobile/ui/Panier.dart';
import '../models/Connection.dart';
import '../viewmodels/task_view_model.dart';
import 'Searchbarre.dart';
import 'home.dart';

class Profil extends StatefulWidget {
  Map<String, String?>? user;
  Connection c;
  Profil(this.c, this.user, {Key? key}) : super(key: key);

  @override
  State<Profil> createState() => _MyStatefulWidgetState(c, user);
}

class _MyStatefulWidgetState extends State<Profil> {
  Map<String, String?>? user;
  Connection c;
  String image = "";
  _MyStatefulWidgetState(this.c, this.user);

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    TextEditingController nameController =
    TextEditingController(text: user!["nom"]);
    TextEditingController passwordController =
    TextEditingController(text: user!["mdp"]);
    TextEditingController avatar = TextEditingController(text: user!["avatar"]);
    TextEditingController textController = TextEditingController();
    TextEditingController mail = TextEditingController(text: user!["email"]);
    TextEditingController passwordController2 =
    TextEditingController(text: user!["mdp"]);
    TaskViewModel listeTodo = context.watch<TaskViewModel>();

    return Scaffold(
        appBar: AppBar(
          actions: <Widget>[
            AnimSearchBar(
              autoFocus: true,
              width: 400,
              textController: textController,
              onSuffixTap: () {
                setState(() {
                  textController.clear();
                });
              },
              onSubmitted: (String) {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) =>
                          Searchbarre(listeTodo.getTasks(), String, c, user),
                    ));
              },
            ),
            IconButton(
              icon: const Icon(
                Icons.favorite,
                color: Colors.pink,
              ),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => Favoris(c, listeTodo, user),
                    ));
              },
            ),
            IconButton(
              icon: const Icon(
                Icons.shopping_cart,
                color: Colors.black,
              ),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => Panier(c, listeTodo, user),
                    ));
              },
            ),
            IconButton(
              icon: const Icon(
                Icons.home,
                color: Colors.grey,
              ),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => Home(c, user),
                    ));
              },
            ),
            Container(
                padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                child: CircleAvatar(
                    backgroundColor: Colors.grey[200],
                    radius: 48,
                    backgroundImage: NetworkImage(user!["avatar"]!)))
          ],
          title: const Text(
            'Application Push',
            style: TextStyle(
                color: Colors.black, fontWeight: FontWeight.w500, fontSize: 30),
          ),
        ),
        body: Form(
            key: _formKey,
            child: Material(
                child: Padding(
                    padding: const EdgeInsets.all(10),
                    child: ListView(
                      children: <Widget>[
                        Container(
                            alignment: Alignment.center,
                            padding: const EdgeInsets.all(10),
                            child: const Text(
                              "Profil",
                              style: TextStyle(fontSize: 20),
                            )),
                        Container(
                          child: Image.network(
                            user!["avatar"]!,
                            height: 200,
                            width: 300,
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.all(10),
                          child: TextFormField(
                            controller: nameController,
                            decoration: const InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: "Nom d'utilisateur",
                              hintText: "Entrer le nom d'utilisateur",
                            ),
                            validator: (String? value) {
                              if (value == null || value.isEmpty) {
                                return "Veuillez entrer le nom d'utilisateur";
                              }
                              return null;
                            },
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.all(10),
                          child: TextFormField(
                            controller: mail,
                            decoration: const InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: "Votre adresse mail",
                              hintText: "Entrer votre adresse mail",
                            ),
                            validator: (String? value) {
                              if (value == null || value.isEmpty) {
                                return "Veuillez entrer votre adresse mail";
                              }
                              return null;
                            },
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.all(10),
                          child: TextField(
                            controller: avatar,
                            decoration: const InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: "Lien de votre avatar",
                              hintText: "Votre image doit commencer par: https",
                            ),
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                          child: TextFormField(
                            obscureText: true,
                            controller: passwordController,
                            decoration: const InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'Mot de passe',
                              hintText: 'Entrer votre mot de passe',
                            ),
                            validator: (String? value) {
                              if (value == null || value.isEmpty) {
                                return 'Veuillez entrer votre mot de passe';
                              }
                              return null;
                            },
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                          child: TextFormField(
                            obscureText: true,
                            controller: passwordController2,
                            decoration: const InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'Confirmer mot de passe',
                              hintText: 'Confirmer votre mot de passe',
                            ),
                            validator: (String? value) {
                              if (value == null || value.isEmpty) {
                                return 'Veuillez confirmer votre mot de passe';
                              }
                              return null;
                            },
                          ),
                        ),
                        Container(
                            height: 50,
                            padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                            child: ElevatedButton(
                              child: const Text('Enregistrer'),
                              onPressed: () {
                                c.updateUser(nameController.text, mail.text,
                                    avatar.text, passwordController.text);
                                var result = c.requet("select * from USER2");
                                get(List<Map<String, String?>?> value) {
                                  int i = 0;
                                  while (i < value.length) {
                                    if (_formKey.currentState!.validate()) {
                                      if (value[i]!["mdp"].toString() ==
                                          passwordController.text) {
                                        Map<String, String?> user = value[i]!;
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                              builder: (context) =>
                                                  Home(c, user),
                                            ));
                                      }
                                    }
                                    i = i + 1;
                                  }
                                }

                                result.then((value) => get(value));
                              },
                            )),
                      ],
                    )))));
  }
}
