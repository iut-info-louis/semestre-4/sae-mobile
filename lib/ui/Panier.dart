// ignore_for_file: sort_child_properties_last, no_logic_in_create_state, library_private_types_in_public_api, must_be_immutable, file_names, unnecessary_brace_in_string_interps

import 'package:anim_search_bar/anim_search_bar.dart';
import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import '../models/Connection.dart';
import 'package:provider/provider.dart';
import '../models/todos.dart';
import '../viewmodels/task_view_model.dart';
import 'Favoris.dart';
import 'Searchbarre.dart';
import 'consultation.dart';
import 'home.dart';

class Panier extends StatefulWidget {
  Connection c;
  TaskViewModel objTasks;
  Map<String, String?>? user;
  Panier(this.c, this.objTasks, this.user, {super.key});

  @override
  _Panier createState() => _Panier(c, objTasks, user);
}

class _Panier extends State<Panier> {
  Connection c;
  TaskViewModel objTasks;
  Map<String, String?>? user;
  _Panier(this.c, this.objTasks, this.user);
  late Future<List<Todo>> futureTodo;
  @override
  void initState() {
    super.initState();
    futureTodo = objTasks.getPanier();
  }

  @override
  Widget build(BuildContext context) {
    TaskViewModel listeTodo = context.watch<TaskViewModel>();
    TextEditingController textController = TextEditingController();
    double total = 0;
    return Scaffold(
        appBar: AppBar(
          actions: <Widget>[
            AnimSearchBar(
              autoFocus: true,
              width: 400,
              textController: textController,
              onSuffixTap: () {
                setState(() {
                  textController.clear();
                });
              },
              onSubmitted: (String) {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) =>
                          Searchbarre(listeTodo.getTasks(), String, c, user),
                    ));
              },
            ),
            IconButton(
              icon: const Icon(
                Icons.favorite,
                color: Colors.pink,
              ),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => Favoris(c, listeTodo, user),
                    ));
              },
            ),
            IconButton(
              icon: const Icon(
                Icons.shopping_cart,
                color: Colors.black,
              ),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => Panier(c, listeTodo, user),
                    ));
              },
            ),
            IconButton(
              icon: const Icon(
                Icons.home,
                color: Colors.grey,
              ),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => Home(c, user),
                    ));
              },
            ),
            Container(
                padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                child: CircleAvatar(
                    backgroundColor: Colors.grey[200],
                    radius: 48,
                    backgroundImage: NetworkImage(user!["avatar"]!)))
          ],
          title: const Text(
            'Lou-lou',
            style: TextStyle(
                color: Colors.black, fontWeight: FontWeight.w500, fontSize: 30),
          ),
        ),
        body: FutureBuilder(
            future: futureTodo,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      ListView.builder(
                          shrinkWrap: true,
                          itemCount: snapshot.data!.length,
                          itemBuilder: (context, index) => Card(
                            elevation: total =
                                total + snapshot.data![index].price,
                            margin: const EdgeInsets.all(10),
                            child: ListTile(
                              leading: CircleAvatar(
                                backgroundColor: Colors.white,
                                child: Image.network(
                                  snapshot.data![index].img,
                                  height: 50,
                                ),
                              ),
                              title: Text(snapshot.data![index].title),
                              trailing: IconButton(
                                iconSize: 30,
                                icon: const Icon(
                                  Icons.delete,
                                  color: Colors.black,
                                ),
                                onPressed: () {
                                  c.requet(
                                      "delete from PANIER where idT = ${snapshot.data![index].id}");
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) =>
                                            Panier(c, listeTodo, user),
                                      ));
                                },
                              ),
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => MyWidget(
                                          c: c,
                                          listeTodos: objTasks,
                                          todo: snapshot.data![index],
                                          user: user),
                                    ));
                              },
                            ),
                          )),
                      Container(
                          height: 50,
                          padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                          child: ElevatedButton(
                            child: const Text('Ajouter au panier'),
                            onPressed: () {
                              Alert(
                                context: context,
                                title: "Validation des achats",
                                desc: "Vous en avez eu pour ${total} euros",
                                buttons: [
                                  DialogButton(
                                      child: const Text(
                                        "Acceuil",
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 20),
                                      ),
                                      onPressed: () {
                                        rm(List<Map<String, String?>?> value) {
                                          int i = 0;
                                          while (i < value.length) {
                                            c.insertHistorique(
                                                int.parse(value[i]!["idT"]!));
                                            i = i + 1;
                                          }
                                          c.requet("delete from PANIER");
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                builder: (context) =>
                                                    Home(c, user),
                                              ));
                                        }

                                        var result =
                                        c.requet("select * from PANIER");
                                        result.then((value) => rm(value));
                                      })
                                ],
                              ).show();
                            },
                          )),
                      const SizedBox(width: 8),
                    ]);
              } else if (snapshot.hasError) {
                return Text('${snapshot.error}');
              } else {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              }
            }));
  }
}
