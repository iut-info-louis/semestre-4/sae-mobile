// ignore_for_file: sort_child_properties_last, no_logic_in_create_state

import 'package:flutter/material.dart';
import 'package:sae_mobile/ui/senregistrer.dart';

import '../models/Connection.dart';
import 'Accueil.dart';
import 'home.dart';
import 'mdpoublie.dart';

class Login extends StatefulWidget {
  Connection c;
  Login(this.c, {Key? key}) : super(key: key);

  @override
  State<Login> createState() => _MyStatefulWidgetState(c);
}

class _MyStatefulWidgetState extends State<Login> {
  Connection c;
  _MyStatefulWidgetState(this.c);
  TextEditingController nameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

    return Material(
        child: Form(
            key: _formKey,
            child: Padding(
                padding: const EdgeInsets.all(10),
                child: ListView(
                  children: <Widget>[
                    Container(
                        alignment: Alignment.center,
                        padding: const EdgeInsets.all(10),
                        child: const Text(
                          'Lou-lou',
                          style: TextStyle(
                              color: Colors.blue,
                              fontWeight: FontWeight.w500,
                              fontSize: 30),
                        )),
                    Container(
                        alignment: Alignment.center,
                        padding: const EdgeInsets.all(10),
                        child: const Text(
                          'Se connecter',
                          style: TextStyle(fontSize: 20),
                        )),
                    Container(
                      padding: const EdgeInsets.all(10),
                      child: TextFormField(
                        controller: nameController,
                        decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: 'Email Utilisateur',
                          hintText: "Entrer votre email d'utilisateur",
                        ),
                        validator: (String? value) {
                          if (value == null || value.isEmpty) {
                            return "Veuillez entrer votre email d'utilisateur";
                          }
                          return null;
                        },
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                      child: TextFormField(
                        obscureText: true,
                        controller: passwordController,
                        decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: 'Mot de passe',
                          hintText: 'Entrer votre mot de passe',
                        ),
                        validator: (String? value) {
                          if (value == null || value.isEmpty) {
                            return 'Veuillez entrer votre mot de passe';
                          }
                          return null;
                        },
                      ),
                    ),
                    TextButton(
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => Mdpoublie(c),
                            ));
                      },
                      child: const Text('Mot de passe oublier',
                          style: TextStyle(fontSize: 15)),
                    ),
                    Container(
                        height: 50,
                        padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                        child: ElevatedButton(
                          child: const Text('Se connecter'),
                          onPressed: () {
                            c.create();
                            var result = c.requet("select * from USER2");
                            get(List<Map<String, String?>?> value) {
                              int i = 0;
                              while (i < value.length) {
                                if (_formKey.currentState!.validate()) {
                                  if (value[i]!["email"].toString() ==
                                      nameController.text) {
                                    if (value[i]!["mdp"].toString() ==
                                        passwordController.text) {
                                      Map<String, String?> user = value[i]!;
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                            builder: (context) => Home(c, user),
                                          ));
                                    }
                                  }
                                }
                                i = i + 1;
                              }
                            }

                            result.then((value) => get(value));
                          },
                        )),
                    Row(
                      children: <Widget>[
                        const Text('Pas encore de compte?'),
                        TextButton(
                          child: const Text(
                            "Créer s'en un",
                            style: TextStyle(fontSize: 15),
                          ),
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => Enregistrement(c),
                                ));
                          },
                        )
                      ],
                      mainAxisAlignment: MainAxisAlignment.center,
                    ),
                  ],
                ))));
  }
}
