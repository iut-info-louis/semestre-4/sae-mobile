// ignore_for_file: sort_child_properties_last

import 'package:flutter/material.dart';

import '../models/Connection.dart';
import 'login.dart';

class Mdpoublie extends StatefulWidget {
  Connection c;
  Mdpoublie(this.c, {Key? key}) : super(key: key);

  @override
  State<Mdpoublie> createState() => _MyStatefulWidgetState(c);
}

class _MyStatefulWidgetState extends State<Mdpoublie> {
  Connection c;
  _MyStatefulWidgetState(this.c);
  TextEditingController nameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController passwordController2 = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: const Text(
            'Lou-lou',
            style: TextStyle(
                color: Colors.black, fontWeight: FontWeight.w500, fontSize: 30),
          ),
        ),
        body: Form(
            key: _formKey,
            child: Material(
                child: Padding(
                    padding: const EdgeInsets.all(10),
                    child: ListView(
                      children: <Widget>[
                        Container(
                            alignment: Alignment.center,
                            padding: const EdgeInsets.all(10),
                            child: const Text(
                              "Changement de mot de passe",
                              style: TextStyle(fontSize: 20),
                            )),
                        Container(
                          padding: const EdgeInsets.all(10),
                          child: TextFormField(
                            controller: nameController,
                            decoration: const InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: "Email d'utilisateur",
                              hintText: "Entrer votre email d'utilisateur",
                            ),
                            validator: (String? value) {
                              if (value == null || value.isEmpty) {
                                return "Veuillez entrer votre email d'utilisateur";
                              }
                              return null;
                            },
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                          child: TextFormField(
                            obscureText: true,
                            controller: passwordController,
                            decoration: const InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'Nouveau mot de passe',
                              hintText: 'Entrer le nouveau mot de passe',
                            ),
                            validator: (String? value) {
                              if (value == null || value.isEmpty) {
                                return 'Veuillez entrer le nouveau mot de passe';
                              }
                              return null;
                            },
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                          child: TextFormField(
                            obscureText: true,
                            controller: passwordController2,
                            decoration: const InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'Confirmer mot de passe',
                              hintText: 'Confirmer le mot de passe',
                            ),
                            validator: (String? value) {
                              if (value == null || value.isEmpty) {
                                return 'Veuillez confirmer le mot de passe';
                              }
                              return null;
                            },
                          ),
                        ),
                        Container(
                            height: 50,
                            padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                            child: ElevatedButton(
                              child: const Text('Enregistrer'),
                              onPressed: () {
                                if (_formKey.currentState!.validate()) {
                                  if (passwordController.text ==
                                      passwordController2.text) {
                                    c.mdpoublie(nameController.text,
                                        passwordController.text);
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) => Login(c),
                                        ));
                                  }
                                }
                              },
                            )),
                        Row(
                          children: <Widget>[
                            TextButton(
                              child: const Text(
                                "Se connecter",
                                style: TextStyle(fontSize: 15),
                              ),
                              onPressed: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => Login(c),
                                    ));
                              },
                            )
                          ],
                          mainAxisAlignment: MainAxisAlignment.center,
                        ),
                      ],
                    )))));
  }
}
