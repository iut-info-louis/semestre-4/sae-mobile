// ignore_for_file: sort_child_properties_last

import 'package:flutter/material.dart';

import '../models/Connection.dart';
import 'login.dart';

class Enregistrement extends StatefulWidget {
  Connection c;
  Enregistrement(this.c, {Key? key}) : super(key: key);

  @override
  State<Enregistrement> createState() => _MyStatefulWidgetState(c);
}

class _MyStatefulWidgetState extends State<Enregistrement> {
  Connection c;
  _MyStatefulWidgetState(this.c);
  TextEditingController nameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController avatar = TextEditingController();
  TextEditingController mail = TextEditingController();
  TextEditingController passwordController2 = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    c.connection();
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: const Text(
            'Lou-lou',
            style: TextStyle(
                color: Colors.black, fontWeight: FontWeight.w500, fontSize: 30),
          ),
        ),
        body: Form(
            key: _formKey,
            child: Material(
                child: Padding(
                    padding: const EdgeInsets.all(10),
                    child: ListView(
                      children: <Widget>[
                        Container(
                            alignment: Alignment.center,
                            padding: const EdgeInsets.all(10),
                            child: const Text(
                              "Creer un compte",
                              style: TextStyle(fontSize: 20),
                            )),
                        Container(
                          padding: const EdgeInsets.all(10),
                          child: TextFormField(
                            controller: nameController,
                            decoration: const InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: "Nom d'utilisateur",
                              hintText: "Entrer le nom d'utilisateur",
                            ),
                            validator: (String? value) {
                              if (value == null || value.isEmpty) {
                                return "Veuillez entrer le nom d'utilisateur";
                              }
                              return null;
                            },
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.all(10),
                          child: TextFormField(
                            controller: mail,
                            decoration: const InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: "Votre adresse mail",
                              hintText: "Entrer votre adresse mail",
                            ),
                            validator: (String? value) {
                              if (value == null || value.isEmpty) {
                                return "Veuillez entrer votre adresse mail";
                              }
                              return null;
                            },
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.all(10),
                          child: TextField(
                            controller: avatar,
                            decoration: const InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: "Lien de votre avatar",
                              hintText: "Votre image doit commencer par: https",
                            ),
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                          child: TextFormField(
                            obscureText: true,
                            controller: passwordController,
                            decoration: const InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'Mot de passe',
                              hintText: 'Entrer votre mot de passe',
                            ),
                            validator: (String? value) {
                              if (value == null || value.isEmpty) {
                                return 'Veuillez entrer votre mot de passe';
                              }
                              return null;
                            },
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                          child: TextFormField(
                            obscureText: true,
                            controller: passwordController2,
                            decoration: const InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'Confirmer mot de passe',
                              hintText: 'Confirmer votre mot de passe',
                            ),
                            validator: (String? value) {
                              if (value == null || value.isEmpty) {
                                return 'Veuillez confirmer votre mot de passe';
                              }
                              return null;
                            },
                          ),
                        ),
                        Container(
                            height: 50,
                            padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                            child: ElevatedButton(
                              child: const Text('Enregistrer'),
                              onPressed: () {
                                c.create();
                                if (_formKey.currentState!.validate()) {
                                  if (passwordController.text ==
                                      passwordController2.text) {
                                    c.insertUser(nameController.text, mail.text,
                                        avatar.text, passwordController.text);
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) => Login(c),
                                        ));
                                  }
                                }
                              },
                            )),
                        Row(
                          children: <Widget>[
                            TextButton(
                              child: const Text(
                                "Se connecter",
                                style: TextStyle(fontSize: 15),
                              ),
                              onPressed: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => Login(c),
                                    ));
                              },
                            )
                          ],
                          mainAxisAlignment: MainAxisAlignment.center,
                        ),
                      ],
                    )))));
  }
}
