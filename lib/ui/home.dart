// ignore_for_file: library_private_types_in_public_api, unused_element, prefer_const_constructors, no_logic_in_create_state, avoid_print

import 'package:flutter/material.dart';
import 'package:anim_search_bar/anim_search_bar.dart';
import 'package:provider/provider.dart';
import '../models/Connection.dart';
import '../models/EcranSettings.dart';
import '../viewmodels/task_view_model.dart';
import './Searchbarre.dart';
import 'Ajoutproduit.dart';
import 'Articles.dart';
import 'Favoris.dart';
import 'Historique.dart';
import 'Panier.dart';

class Home extends StatefulWidget {
  Map<String, String?>? user;
  late Connection c;
  Home(this.c, this.user, {super.key});
  @override
  _HomeState createState() => _HomeState(c, user);
}

class _HomeState extends State<Home> {
  Map<String, String?>? user;
  late Connection c;
  _HomeState(this.c, this.user);
  int _selectedIndex = 0;
  String image = "";
  TextEditingController textController = TextEditingController();

  // factory _HomeState.constructeurAvecListeTask(this.liste_tasks) {
  //  this.liste_tasks
  //}
  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    TaskViewModel listeTodo = context.watch<TaskViewModel>();

    List<Widget> pages = <Widget>[
      Articles(c, listeTodo, user),
      Historique(c, listeTodo, user),
      Ajoutproduit(c, user!),
      EcranSettings(c, user)
    ];
    TextEditingController textController = TextEditingController();
    return Scaffold(
      appBar: AppBar(
        actions: <Widget>[
          AnimSearchBar(
            autoFocus: true,
            width: 400,
            textController: textController,
            onSuffixTap: () {
              setState(() {
                textController.clear();
              });
            },
            onSubmitted: (String) {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) =>
                        Searchbarre(listeTodo.getTasks(), String, c, user),
                  ));
            },
          ),
          IconButton(
            icon: Icon(
              Icons.favorite,
              color: Colors.pink,
            ),
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => Favoris(c, listeTodo, user),
                  ));
            },
          ),
          IconButton(
            icon: Icon(
              Icons.shopping_cart,
              color: Colors.black,
            ),
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => Panier(c, listeTodo, user),
                  ));
            },
          ),
          IconButton(
            icon: Icon(
              Icons.home,
              color: Colors.grey,
            ),
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => Home(c, user),
                  ));
            },
          ),
          Container(
              padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: CircleAvatar(
                  backgroundColor: Colors.grey[200],
                  radius: 48,
                  backgroundImage: NetworkImage(user!["avatar"]!)))
        ],
        title: Text(
          'Lou-lou',
          style: TextStyle(
              color: Colors.black, fontWeight: FontWeight.w500, fontSize: 30),
        ),
      ),
      body: pages[_selectedIndex],
      bottomNavigationBar: BottomNavigationBar(
        unselectedItemColor: Colors.black,
        selectedItemColor: Theme.of(context).textSelectionTheme.selectionColor,
        currentIndex: _selectedIndex,
        onTap: _onItemTapped,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(
              Icons.shopping_bag,
              color: Colors.blue,
            ),
            label: "Articles",
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.history_sharp,
              color: Colors.orange,
            ),
            label: "Historique",
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.add,
              color: Colors.green,
            ),
            label: "Ajouter produit",
          ),
          BottomNavigationBarItem(
              icon: Icon(
                Icons.settings,
                color: Colors.grey,
              ),
              label: 'Settings')
        ],
      ),
    );
  }
}
