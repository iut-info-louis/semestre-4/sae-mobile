// ignore_for_file: sort_child_properties_last, must_be_immutable, unnecessary_const, unnecessary_new, camel_case_types

import 'package:anim_search_bar/anim_search_bar.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sae_mobile/viewmodels/task_view_model.dart';
import '../api/myapi.dart';
import '../models/Connection.dart';
import '../models/todos.dart';
import 'Favoris.dart';
import 'Panier.dart';
import 'Searchbarre.dart';
import 'home.dart';

class modifier extends StatefulWidget {
  Map<String, String?>? user;
  late Todo data;
  Connection c;
  modifier(this.data, this.c, this.user, {super.key});

  @override
  _modifier createState() => _modifier(data, c, user);
}

class _modifier extends State<modifier> {
  Map<String, String?>? user;
  Todo data;
  Connection c;
  _modifier(this.data, this.c, this.user);

  @override
  Widget build(BuildContext context) {
    TextEditingController textController = TextEditingController();
    TextEditingController nom = TextEditingController(text: data.title);
    TextEditingController desc = TextEditingController(text: data.description);
    TextEditingController prix =
    TextEditingController(text: "${data.price.toString()} euros");
    TextEditingController avis = TextEditingController(
      text: "${data.rate} sur ${data.count.toString()} avis",
    );
    TextEditingController img = TextEditingController(text: data.img);
    TaskViewModel listeTodo = context.watch<TaskViewModel>();

    final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
    return Scaffold(
        appBar: AppBar(
          actions: <Widget>[
            AnimSearchBar(
              autoFocus: true,
              width: 400,
              textController: textController,
              onSuffixTap: () {
                setState(() {
                  textController.clear();
                });
              },
              onSubmitted: (String) {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) =>
                          Searchbarre(listeTodo.getTasks(), String, c, user),
                    ));
              },
            ),
            IconButton(
              icon: const Icon(
                Icons.favorite,
                color: Colors.pink,
              ),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => Favoris(c, listeTodo, user),
                    ));
              },
            ),
            IconButton(
              icon: const Icon(
                Icons.shopping_cart,
                color: Colors.black,
              ),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => Panier(c, listeTodo, user),
                    ));
              },
            ),
            IconButton(
              icon: const Icon(
                Icons.home,
                color: Colors.grey,
              ),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => Home(c, user),
                    ));
              },
            ),
            Container(
                padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                child: CircleAvatar(
                  backgroundColor: Colors.grey[200],
                  radius: 48,
                  backgroundImage: NetworkImage(user!['avatar']!),
                )),
          ],
          title: const Text(
            'Lou-lou',
            style: TextStyle(
                color: Colors.black, fontWeight: FontWeight.w500, fontSize: 30),
          ),
        ),
        body: Form(
            key: _formKey,
            child: Material(
                child: Padding(
                    padding: const EdgeInsets.all(10),
                    child: ListView(
                      children: <Widget>[
                        Container(
                            alignment: Alignment.center,
                            padding: const EdgeInsets.all(10),
                            child: const Text(
                              'Ajouter produit',
                              style: TextStyle(fontSize: 20),
                            )),
                        Container(
                          child: Image.network(
                            img.text,
                            height: 200,
                            width: 300,
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.all(10),
                          child: TextFormField(
                            controller: nom,
                            decoration: const InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'Nom du produit',
                              hintText: 'Entrer le nom du produit',
                            ),
                            validator: (String? value) {
                              if (value == null || value.isEmpty) {
                                return 'Veuillez entrer le nom du produit';
                              }
                              return null;
                            },
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                          child: TextFormField(
                            minLines: 6,
                            maxLines: null,
                            controller: desc,
                            keyboardType: TextInputType.multiline,
                            decoration: const InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'Description du produit',
                              hintText: 'Entrer une description du produit',
                            ),
                            validator: (String? value) {
                              if (value == null || value.isEmpty) {
                                return 'Veuillez entrer une description du produit';
                              }
                              return null;
                            },
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.all(10),
                          child: TextFormField(
                            decoration: const InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'Prix du produit',
                              hintText: 'Entrer le prix du produit',
                            ),
                            validator: (String? value) {
                              if (value == null || value.isEmpty) {
                                return 'Veuillez entrer le prix du produit';
                              }
                              return null;
                            },
                            controller: prix,
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.all(10),
                          child: TextFormField(
                            enabled: false,
                            decoration: const InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'Avis du produit',
                              hintText: "Entrer le nombre d'avis du produit",
                            ),
                            validator: (String? value) {
                              if (value == null || value.isEmpty) {
                                return "Veuillez entrer le nombre d'avis du produit";
                              }
                              return null;
                            },
                            controller: avis,
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.all(10),
                          child: TextFormField(
                            decoration: const InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: "Lien de l'image",
                              hintText: "Entrer le lien de l'image du produit",
                            ),
                            validator: (String? value) {
                              if (value == null || value.isEmpty) {
                                return "Veuillez entrer le lien de l'image du produit";
                              }
                              return null;
                            },
                            controller: img,
                          ),
                        ),
                        Container(
                          height: 50,
                          padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                          child: ElevatedButton(
                            onPressed: () {
                              if (_formKey.currentState!.validate()) {
                                prix.text = (prix.text.substring(0, prix.text.length-6));
                                MyAPI.mofifierTodo(data.id, nom.text, desc.text,
                                    img.text, double.parse(prix.text));
                                data.modifier(nom.text, desc.text, double.parse(prix.text), 1, 1);
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) =>
                                          Home(c, user),
                                    ));
                              }
                            },
                            child: const Text('Enregistrer'),
                          ),
                        ),
                      ],
                    )))));
  }
}
